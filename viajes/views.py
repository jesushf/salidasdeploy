#You can create your views here
from django.contrib.auth import authenticate, login, logout
from rest_framework import generics, permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from viajes.models import *
from viajes.serializers import *
from django.contrib import auth
from viajes.permissions import OAuthObjectPermissions
from viajes.permissionusername import Permissions
from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse, HttpResponseRedirect
from reportlab.lib.pagesizes import letter
import datetime
from reportlab.platypus import Paragraph, SimpleDocTemplate, Image, Table, TableStyle
from reportlab.lib.styles import ParagraphStyle
from django.core.files.storage import FileSystemStorage
from reportlab.lib.units import inch
from reportlab.lib.enums import *
from django.views.generic import View
import json


def pdf2(request,id):


    lista = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
             "Noviembre", "Diciembre"]
    trip = Trip.objects.get(id=id)
    reason = str(trip.reasons)
    leader = str(trip.groupLeader)
    destinations = str(trip.destinations)
    passengers=str(trip.passengers)
    dia = str(trip.startingDate.day)
    mes = str(lista[trip.startingDate.month - 1])
    año = str(trip.startingDate.year)
    diaf = str(trip.finishingDate.day)
    mesf = str(lista[trip.finishingDate.month - 1])
    añof = str(trip.finishingDate.year)
    startingtime = str(trip.startingTime)
    startingtime = startingtime[:-3]
    departure = str(trip.departurePlace)
    idd = trip.department
    dep = Department.objects.get(name=idd)
    dboss = str(dep.boss).upper()
    dname = str(dep.name).upper()
    ids = dep.submanagement
    sub = SubManagement.objects.get(name=ids)
    sboss = str(sub.boss).upper()
    sname = str(sub.name).upper()
    doc = SimpleDocTemplate("/tmp/solicitud.pdf",leftMargin=0.75*inch, rightMargin=0.75*inch, topMargin=0.3*inch, bottomMargin=0.2*inch)
    obps = ParagraphStyle(name='default', fontName="Courier", fontSize=9, leading=10, alignment=4)
    obpm = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_RIGHT)
    obpl = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_LEFT)
    obpc = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_CENTER)
    #leading el tamaño del entrelineado, alignment puede tener un valor del 0 al 4, el 4 representa justificar
    today = datetime.datetime.today()
    dat = str(today.day) + "/" + str(lista[today.month - 1]) + "/" + str(today.year)
    im1 = Image("salidad/imgs/sep.jpg", 188, 128)
    im2 = Image("salidad/imgs/escudo.jpg", 30, 30)
    im3 = Image("salidad/imgs/iso9001.jpg", 51, 23)
    im4 = Image("salidad/imgs/iso14001.png", 48, 21)
    data=[[im1,(Paragraph('''<b><font size='12'>TECNOLÓGICO NACIONAL DE MÉXICO</font></b><br/>Instituto Tecnológico de Jiquilpan''',obpm, bulletText=None))],
          ['',''],
          [(Paragraph('''<b>ING. LEONARDO MARTÍNEZ GONZÁLEZ<br/>SUBDIRECTOR ADMINISTRATIVO<br/>PRESENTE</b>''',obpl, bulletText=None)),(Paragraph('''Jiquilpan,Mich., '''+dat+'''<br/>Asunto: Solicitud de transporte''',obpm, bulletText=None))],
          ['','']]

    t=Table(data)
    t.setStyle(TableStyle([('SPAN', (0, 0), (-2, -3)),
                           ('ALIGN', (0, 0), (-2, -3), 'LEFT'),
                           ('VALIGN', (0, 0), (-2, -3), 'TOP'),
                           ('LEFTPADDING', (0, 0), (-2, -3), -12),
                           ('TOPPADDING', (0, 0), (-2, -3), -30),
                           ('SPAN', (1, 0), (-1, -3)),
                           ('ALIGN', (1, 0), (-1, -3), 'RIGHT'),
                           ('VALIGN', (1, 0), (-1, -3), 'TOP'),
                           ('RIGHTPADDING', (1, 0), (-1, -3), 0),
                           ('TOPPADDING', (1, 0), (-1, -3), 20),
                           ('SPAN', (1, 2), (-1, -1)),
                           ('ALIGN', (1, 2), (-1, -1), 'RIGHT'),
                           ('VALIGN', (1, 2), (-1, -1), 'TOP'),
                           ('RIGHTPADDING', (1, 2), (-1, -1), 0),
                           ('TOPPADDING', (1, 2), (-1, -1), -35),
                           ('SPAN', (0, 2), (-2, -1)),
                           ('ALIGN', (0, 2), (-2, -1), 'LEFT'),
                           ('VALIGN', (0, 2), (-2, -1), 'BOTTOM'),
                           ('LEFTPADDING', (0, 2), (-2, -1), 0),
                           ('TOPPADDING', (0, 2), (-2, -1),10),]))
    #valign para la altura, align para la anchura
    par = []
    par.append(t)
    par.append(Paragraph(
        '''<br/>Por este conducto reciba un cordial saludo; así mismo le solicito de la manera más atenta el servicio de traslado vehicular para '''+passengers+''' personas, a cargo de '''+leader+
        ''', a la ciudad de '''+destinations+''' para '''+reason+''', que tendrá lugar el día '''+dia+''' de '''+mes+''' del año '''+año+''' y terminará el '''+diaf+''' de '''+mesf+''' del año '''+añof+
        ''' con salida a las '''+startingtime+''' horas, partiendo de '''+departure+'''.''',
        obps, bulletText=None))
    par.append(Paragraph(
        '''<br/><br/><br/>Agradeciendo de antemano la atención que se sirva al presente, aprovecho la ocasión para reiterarme a sus distinguidas órdenes.''',
        obps, bulletText=None))
    par.append(Paragraph(
        '''<br/><br/><br/><br/><br/><b>ATENTAMENTE<br/><font size='8'>Justicia Social en la Tecnificación Industrial</font></b>''',
        obpl, bulletText=None))
    data2 = [[(Paragraph('''<br/><br/><br/><br/><br/><br/>_________________________________________<br/><b>'''+dboss+'''<br/>JEFE DEL DEPTO. DE '''+dname+'''</b>''',obpc, bulletText=None)),'',
              (Paragraph('''<br/><br/><br/><br/><br/><br/>_________________________________________<br/><b>'''+sboss+'''<br/>SUBDIRECCIÓN '''+sname+'''</b>''',obpc, bulletText=None)),''],
            ['', '','',''],]

    t2 = Table(data2)
    t2.setStyle(TableStyle([('SPAN', (0, 0), (-3, -1)),
                           ('ALIGN', (0, 0), (-3, -1), 'RIGHT'),
                           ('VALIGN', (0, 0), (-3, -1), 'TOP'),
                           ('LEFTPADDING', (0, 0), (-3, -1), 20),
                           ('TOPPADDING', (0, 0), (-3, -1), 0),
                           ('SPAN', (2, 0), (-1, -1)),
                           ('ALIGN', (2, 0), (-1, -1), 'LEFT'),
                           ('VALIGN', (2, 0), (-1, -1), 'TOP'),
                           ('RIGHTPADDING', (2, 0), (-1, -1), 20),
                           ('TOPPADDING', (2, 0), (-1, -1), 0),]))
    par.append(t2)
    is9=Paragraph("RSGC 568",obpc,bulletText=None)
    is14 = Paragraph("RSGA 059", obpc, bulletText=None)
    data3=[[im2,'',(Paragraph('''<font size='8'>Av. Carr. Nacional S/N km 202 CP. 59510<br/>Jiquilpan, Mich. Tels. (353) 533 30 91, 533 11 26  Ext. 201<br/><u>www.itjiquilpan.edu.mx</u></font>''', obpc, bulletText=None)),'','','','','',[im3,is9], [im4,is14]]]
    t3=Table(data3)
    t3.setStyle(TableStyle([('SPAN', (0, 0), (-9, -1)),
                           ('ALIGN', (0, 0), (-9, -1), 'LEFT'),
                           ('VALIGN', (0, 0), (-9, -1), 'BOTTOM'),
                           ('LEFTPADDING', (0, 0), (-9, -1), 0),
                           ('TOPPADDING', (0, 0), (-9, -1), 270),
                           ('SPAN', (2, 0), (-3, -1)),
                           ('ALIGN', (2, 0), (-3, -1), 'CENTER'),
                           ('VALIGN', (2, 0), (-3, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (2, 0), (-3, -1), 0),
                           ('TOPPADDING', (2, 0), (-3, -1), 270),
                           ('ALIGN', (8, 0), (-2, -1), 'RIGHT'),
                           ('VALIGN', (8, 0), (-2, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (8, 0), (-2, -1), 0),
                           ('TOPPADDING', (8, 0), (-2, -1), 270),
                           ('ALIGN', (9, 0), (-1, -1), 'RIGHT'),
                           ('VALIGN', (9, 0), (-1, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (9, 0), (-1, -1), 0),
                           ('TOPPADDING', (9, 0), (-1, -1), 270),]))
    par.append(t3)
    doc.build(par)
    fs = FileSystemStorage("/tmp")
    with fs.open("solicitud.pdf") as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="solicitud.pdf"'
        return response


def pdf(request,id):
    lista = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
             "Noviembre", "Diciembre"]
    trip=Trip.objects.get(id=id)
    reason=str(trip.reasons)
    destinations=str(trip.destinations)
    ids=trip.submanagement
    passengers=str(trip.passengers)
    sub=SubManagement.objects.get(name=ids)
    sboss=str(sub.boss).upper()
    sname=str(sub.name).upper()
    dia = str(trip.startingDate.day)
    mes = str(lista[trip.startingDate.month - 1])
    año = str(trip.startingDate.year)
    startingtime = str(trip.startingTime)
    startingtime = startingtime[:-3]
    departure = str(trip.departurePlace)
    diaf = str(trip.finishingDate.day)
    mesf = str(lista[trip.finishingDate.month - 1])
    añof = str(trip.finishingDate.year)
    leader=str(trip.groupLeader)

    doc = SimpleDocTemplate("/tmp/solicitud.pdf",leftMargin=0.75*inch, rightMargin=0.75*inch, topMargin=0.3*inch, bottomMargin=0.2*inch)
    obps = ParagraphStyle(name='default', fontName="Courier", fontSize=9, leading=10, alignment=4)
    obpm = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_RIGHT)
    obpl = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_LEFT)
    obpc = ParagraphStyle(name='bold', fontName="Courier", fontSize=9, leading=10, alignment=TA_CENTER)
    #leading el tamaño del entrelineado, alignment puede tener un valor del 0 al 4, el 4 representa justificar
    today = datetime.datetime.today()
    dat = str(today.day) + "/" + str(lista[today.month - 1]) + "/" + str(today.year)
    im1 = Image("salidad/imgs/sep.jpg", 188, 128)
    im2 = Image("salidad/imgs/escudo.jpg", 30, 30)
    im3 = Image("salidad/imgs/iso9001.jpg", 51, 23)
    im4 = Image("salidad/imgs/iso14001.png", 48, 21)
    data=[[im1,(Paragraph('''<b><font size='12'>TECNOLÓGICO NACIONAL DE MÉXICO</font></b><br/>Instituto Tecnológico de Jiquilpan''',obpm, bulletText=None))],
          ['',''],
          [(Paragraph('''<b>ING. LEONARDO MARTÍNEZ GONZÁLEZ<br/>SUBDIRECTOR ADMINISTRATIVO<br/>PRESENTE</b>''',obpl, bulletText=None)),(Paragraph('''Jiquilpan,Mich., '''+dat+'''<br/>Asunto: Solicitud de transporte''',obpm, bulletText=None))],
          ['','']]

    t=Table(data)
    t.setStyle(TableStyle([('SPAN', (0, 0), (-2, -3)),
                           ('ALIGN', (0, 0), (-2, -3), 'LEFT'),
                           ('VALIGN', (0, 0), (-2, -3), 'TOP'),
                           ('LEFTPADDING', (0, 0), (-2, -3), -12),
                           ('TOPPADDING', (0, 0), (-2, -3), -30),
                           ('SPAN', (1, 0), (-1, -3)),
                           ('ALIGN', (1, 0), (-1, -3), 'RIGHT'),
                           ('VALIGN', (1, 0), (-1, -3), 'TOP'),
                           ('RIGHTPADDING', (1, 0), (-1, -3), 0),
                           ('TOPPADDING', (1, 0), (-1, -3), 20),
                           ('SPAN', (1, 2), (-1, -1)),
                           ('ALIGN', (1, 2), (-1, -1), 'RIGHT'),
                           ('VALIGN', (1, 2), (-1, -1), 'TOP'),
                           ('RIGHTPADDING', (1, 2), (-1, -1), 0),
                           ('TOPPADDING', (1, 2), (-1, -1), -35),
                           ('SPAN', (0, 2), (-2, -1)),
                           ('ALIGN', (0, 2), (-2, -1), 'LEFT'),
                           ('VALIGN', (0, 2), (-2, -1), 'BOTTOM'),
                           ('LEFTPADDING', (0, 2), (-2, -1), 0),
                           ('TOPPADDING', (0, 2), (-2, -1),10),]))
    #valign para la altura, align para la anchura
    par = []
    par.append(t)
    par.append(Paragraph(
        '''<br/>Por este conducto reciba un cordial saludo; así mismo le solicito de la manera más atenta el servicio de traslado vehicular para ''' + passengers + ''' personas, a cargo de ''' + leader +
        ''', a la ciudad de ''' + destinations + ''' para ''' + reason + ''', que tendrá lugar el día ''' + dia + ''' de ''' + mes + ''' del año ''' + año + ''' y terminará el ''' + diaf + ''' de ''' + mesf + ''' del año ''' + añof +
        ''' con salida a las ''' + startingtime + ''' horas, partiendo de ''' + departure + '''.''',
        obps, bulletText=None))
    par.append(Paragraph(
        '''<br/><br/><br/>Agradeciendo de antemano la atención que se sirva al presente, aprovecho la ocasión para reiterarme a sus distinguidas órdenes.''',
        obps, bulletText=None))
    par.append(Paragraph(
        '''<br/><br/><br/><br/><br/><b>ATENTAMENTE<br/><font size='8'>Justicia Social en la Tecnificación Industrial</font></b>''',
        obpl, bulletText=None))
    data2 = [['','',
              (Paragraph('''<br/><br/><br/><br/><br/><br/>_________________________________________<br/><b>'''+sboss+'''<br/>SUBDIRECCIÓN '''+sname+'''</b>''',obpc, bulletText=None)),''],
            ['', '','',''],]

    t2 = Table(data2)
    t2.setStyle(TableStyle([('SPAN', (0, 0), (-3, -1)),
                           ('ALIGN', (0, 0), (-3, -1), 'RIGHT'),
                           ('VALIGN', (0, 0), (-3, -1), 'TOP'),
                           ('LEFTPADDING', (0, 0), (-3, -1), 20),
                           ('TOPPADDING', (0, 0), (-3, -1), 0),
                           ('SPAN', (2, 0), (-1, -1)),
                           ('ALIGN', (2, 0), (-1, -1), 'LEFT'),
                           ('VALIGN', (2, 0), (-1, -1), 'TOP'),
                           ('RIGHTPADDING', (2, 0), (-1, -1), 20),
                           ('TOPPADDING', (2, 0), (-1, -1), 0),]))
    par.append(t2)
    is9=Paragraph("RSGC 568",obpc,bulletText=None)
    is14 = Paragraph("RSGA 059", obpc, bulletText=None)
    data3=[[im2,'',(Paragraph('''<font size='8'>Av. Carr. Nacional S/N km 202 CP. 59510<br/>Jiquilpan, Mich. Tels. (353) 533 30 91, 533 11 26  Ext. 201<br/><u>www.itjiquilpan.edu.mx</u></font>''', obpc, bulletText=None)),'','','','','',[im3,is9], [im4,is14]]]
    t3=Table(data3)
    t3.setStyle(TableStyle([('SPAN', (0, 0), (-9, -1)),
                           ('ALIGN', (0, 0), (-9, -1), 'LEFT'),
                           ('VALIGN', (0, 0), (-9, -1), 'BOTTOM'),
                           ('LEFTPADDING', (0, 0), (-9, -1), 0),
                           ('TOPPADDING', (0, 0), (-9, -1), 270),
                           ('SPAN', (2, 0), (-3, -1)),
                           ('ALIGN', (2, 0), (-3, -1), 'CENTER'),
                           ('VALIGN', (2, 0), (-3, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (2, 0), (-3, -1), 0),
                           ('TOPPADDING', (2, 0), (-3, -1), 270),
                           ('ALIGN', (8, 0), (-2, -1), 'RIGHT'),
                           ('VALIGN', (8, 0), (-2, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (8, 0), (-2, -1), 0),
                           ('TOPPADDING', (8, 0), (-2, -1), 270),
                           ('ALIGN', (9, 0), (-1, -1), 'RIGHT'),
                           ('VALIGN', (9, 0), (-1, -1), 'BOTTOM'),
                           ('RIGHTPADDING', (9, 0), (-1, -1), 0),
                           ('TOPPADDING', (9, 0), (-1, -1), 270),]))
    par.append(t3)
    doc.build(par)
    fs = FileSystemStorage("/tmp")
    with fs.open("solicitud.pdf") as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="solicitud.pdf"'
        return response


class SubmanagementList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = SubManagement.objects.all()
    serializer_class = SubManagementSerializer


class SubmanagementDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = SubManagement.objects.all()
    serializer_class = SubManagementSerializer


class DepartmentList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class DepartmentDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer

@api_view(['GET'])
def username(request):
    user = request.user
    return Response({
        'username': user.username,
    })


class UserList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = auth.get_user_model().objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = auth.get_user_model().objects.all()
    serializer_class = UserSerializer

class TripListUser(generics.ListCreateAPIView):
    def get_queryset(self):
        return Trip.objects.filter(createdBy=self.request.user)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, OAuthObjectPermissions)
    serializer_class = TripSerializer


class TripList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Trip.objects.all()
    serializer_class = TripSerializer


class TripDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Trip.objects.all()
    serializer_class = TripSerializer


class VehicleList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer


class VehicleDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer


class DriverTripDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = DriverTrip.objects.all()
    serializer_class = DriverTripSerializer


class DriverTripList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = DriverTrip.objects.all()
    serializer_class = DriverTripSerializer


class MaintenanceDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Maintenance.objects.all()
    serializer_class = MaintenanceSerializer


class MaintenanceList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Maintenance.objects.all()
    serializer_class = MaintenanceSerializer


class PolicyList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class PolicyDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class TripListDep(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        return Trip.objects.filter(department=self.request.user.department)
    serializer_class = TripSerializer


class TripListSub(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        return Trip.objects.filter(submanagement=self.request.user.subManagement).exclude(createdBy=self.request.user)
    serializer_class = TripSerializer

class LoginStandar(APIView):
    def post(self, request, format=None):
        #data = json.loads(request.body)
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'standar':
                if account.is_active:
                    login(request, account)

                    serialized = UserSerializer(account)

                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)
class LoginSub(APIView):
    def post(self, request, format=None):
        #data = json.loads(request.body)
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'sub':
                if account.is_active:
                    login(request, account)

                    serialized = UserSerializer(account)

                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LoginDep(APIView):
    def post(self, request, format=None):
        #data = json.loads(request.body)
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'dep':
                if account.is_active:
                    login(request, account)

                    serialized = UserSerializer(account)

                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LoginAdmin(APIView):
    def post(self, request, format=None):
        #data = json.loads(request.body)
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'admin':
                if account.is_active:
                    login(request, account)

                    serialized = UserSerializer(account)

                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LoginDriver(APIView):
    def post(self, request, format=None):
        #data = json.loads(request.body)
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'driver':
                if account.is_active:
                    login(request, account)

                    serialized = UserSerializer(account)

                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LogoutView(APIView):
    def post(self, request, format=None):
        logout(request)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

class LoginRecView(APIView):
    def post(self, request, format=None):
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        account = authenticate(username=username, password=password)
        if account is not None:
            user = auth.get_user_model().objects.get(username=username)
            if user.chargeAtDepartment == 'recursos':
                vehicles = Vehicle.objects.all()
                for vehicle in vehicles:
                    try:
                        maintenances = Maintenance.objects.filter(vehicle=vehicle.id)
                        if maintenances:
                            maintenance = maintenances[0]
                            for main in maintenances:
                                if main.finishingDate > maintenance.finishingDate:
                                    maintenance=main
                            diff = datetime.date.today() - maintenance.finishingDate
                            if diff.days > 187:
                                try:
                                    notification = Notification.objects.get(maintenance=maintenance.id)
                                except Notification.DoesNotExist:
                                    dic = {'to':'recursos', 'description': 'Vehículo con último mantenimiento hace 6 meses',
                                        'vehicle': vehicle.id, 'maintenance': maintenance.id}
                                    serializer = NotificationSerializer(data=dic)
                                    if serializer.is_valid():
                                        serializer.save()
                    except Maintenance.DoesNotExist:
                        pass
                policies = Policy.objects.all()
                for policy in policies:
                    if policy.finishingDate <= datetime.date.today():
                        try:
                            notification = Notification.objects.get(policy=policy.id)
                        except Notification.DoesNotExist:
                            dic = {'to':'recursos', 'description': 'Póliza vencida',
                               'vehicle': policy.vehicle.id, 'policy': policy.id}
                            serializer = NotificationSerializer(data=dic)
                            if serializer.is_valid():
                                serializer.save()
                if account.is_active:
                    login(request, account)
                    serialized = UserSerializer(account)
                    return Response(serialized.data)
                else:
                    return Response({
                        'status': 'Unauthorized',
                        'message': 'This user cannot login to this site.'
                    }, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Username/password combination invalid.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)


class ListAvalaibleVehicle(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request, format=None):
        data=request.data
        ID = data.get('trip', None)
        if ID:
            tri = Trip.objects.get(id=ID)
            sdate = datetime.datetime.combine(tri.startingDate, tri.startingTime)
            fdate = datetime.datetime.combine(tri.finishingDate, tri.finishingTime)
            drivertrips=DriverTrip.objects.all()
            toremove=[]
            for drivertrip in drivertrips:
                trip = Trip.objects.get(id=drivertrip.trip.id)
                tsdate = datetime.datetime.combine(trip.startingDate, trip.startingTime)
                tfdate = datetime.datetime.combine(trip.finishingDate, trip.finishingTime)
                if tsdate <= sdate <= tfdate:
                    toremove.append(drivertrip.vehicle.id)
                elif tsdate <= fdate <= tfdate:
                    toremove.append(drivertrip.vehicle.id)

            vehicles = Vehicle.objects.exclude(id__in=toremove)
        else:
            return Response({
                'status': 'Bad Request',
                'message': 'No trip selected.'
            }, status=status.HTTP_400_BAD_REQUEST)
        serializer = VehicleSerializer(vehicles, many=True)
        return Response(serializer.data)


class ListAvalaibleDriver(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request, format=None):
        data=request.data
        ID = data.get('trip', None)
        if ID:
            tri = Trip.objects.get(id=ID)
            sdate = datetime.datetime.combine(tri.startingDate, tri.startingTime)
            fdate = datetime.datetime.combine(tri.finishingDate, tri.finishingTime)
            drivertrips=DriverTrip.objects.all()
            toremove=[]
            for drivertrip in drivertrips:
                trip = Trip.objects.get(id=drivertrip.trip.id)
                tsdate = datetime.datetime.combine(trip.startingDate, trip.startingTime)
                tfdate = datetime.datetime.combine(trip.finishingDate, trip.finishingTime)
                if tsdate <= sdate <= tfdate:
                    toremove.append(drivertrip.driver.id)
                elif tsdate <= fdate <= tfdate:
                    toremove.append(drivertrip.driver.id)

            drivers = auth.get_user_model().objects.filter(chargeAtDepartment='driver').exclude(id__in=toremove)
        else:
            return Response({
                'status': 'Bad Request',
                'message': 'No trip selected.'
            }, status=status.HTTP_400_BAD_REQUEST)
        serializer = UserSerializer(drivers, many=True)
        return Response(serializer.data)


class NotificationListUser(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user.id)
    serializer_class = NotificationSerializer


class NotificationList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer


class NotificationDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer


class TripListRecursos(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        trips=Trip.objects.filter(departmentPermission='Si',submanagementPermission='Si')
        drivertrips=DriverTrip.objects.all()
        toremove=[]
        for trip in trips:
            for drivertrip in drivertrips:
                if drivertrip.trip.id == trip.id:
                    toremove.append(trip.id)
        return trips.exclude(id__in=toremove)
    serializer_class = TripSerializer


class DriverTripListUser(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        return DriverTrip.objects.filter(driver=self.request.user.id)
    serializer_class = DriverTripSerializer