from .models import *
from django.contrib import admin
from django.contrib import auth
#from django.contrib.auth.models import User
# Register your models here.
admin.site.register(SubManagement)
admin.site.register(Department)
#admin.site.register(Driver)
#admin.site.register(auth.get_user_model())
admin.site.register(Vehicle)
admin.site.register(Policy)
admin.site.register(Trip)
admin.site.register(DriverTrip)
admin.site.register(Maintenance)
admin.site.register(Site)
admin.site.register(Group)
admin.site.register(Permission)
