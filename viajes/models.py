import uuid
import string
from django.db import models
from django.contrib.auth.models import UserManager, PermissionsMixin, AbstractBaseUser, GroupManager, \
    PermissionManager
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.hashers import make_password
from django.utils.encoding import python_2_unicode_compatible
from django.core import validators
from django.db import models
from django.utils import six, timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib import auth
from rest_framework.exceptions import PermissionDenied, ValidationError
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.conf import settings
# Create your models here.
class AbstractModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    createdAt = models.DateTimeField(blank=True, null=True)


    class Meta:
        abstract = True
        ordering = ['-createdAt']

    def __str__(self):
        return str(self.id)

class Dates(models.Model):
    startingDate = models.DateField()
    finishingDate = models.DateField(null=True, blank=True)
    class Meta:
         abstract = True

class DatesTimes(Dates):
    startingTime = models.TimeField()
    finishingTime = models.TimeField(null=True, blank=True)
    class Meta:
         abstract = True

class SubManagement(AbstractModel):
    name = models.CharField(max_length=30, null=False, blank=False, unique=True)
    boss = models.CharField(max_length=70, null=False, blank=False, unique=True)
    def __str__(self):
        return self.name


class Department(AbstractModel):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    boss = models.CharField(max_length=70, null=False, blank=False, unique=True)
    submanagement = models.ForeignKey(SubManagement, on_delete=models.CASCADE)
    def __str__(self):
        return self.name


def _no_spaces(value):
    """
    Validates that the given value contains no whitespaces to prevent common
    typos.
    """
    if not value:
        return
    has = ((s in value) for s in string.whitespace)
    if any(has):
        raise ValidationError(_("The domain name cannot contain any spaces or tabs."))


class Site(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    logo = models.CharField(max_length=254, null=True, blank=True)
    name = models.CharField(max_length=128)
    slug = models.SlugField(unique=True)
    excerpt = models.CharField(max_length=200, null=True, blank=True)
    domain = models.CharField(
        max_length=100,
        validators=[_no_spaces],
        unique=True,
    )
    #email = models.EmailField()
    #phone1 = models.CharField(max_length=20, null=True, blank=True)
    #phone2 = models.CharField(max_length=20, null=True, blank=True)
    metadata = JSONField(null=True, blank=True)
    isActive = models.BooleanField(default=True)
    deployAt = models.DateField(null=True, blank=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


# A few helper functions for common logic between User and AnonymousUser.
def _user_get_all_permissions(user, obj):
    permissions = set()
    for backend in auth.get_backends():
        if hasattr(backend, "get_all_permissions"):
            permissions.update(backend.get_all_permissions(user, obj))
    return permissions


def _user_has_perm(user, perm, obj):
    """
    A backend can raise `PermissionDenied` to short-circuit permission checking.
    """
    for backend in auth.get_backends():
        if not hasattr(backend, 'has_perm'):
            continue
        try:
            if backend.has_perm(user, perm, obj):
                return True
        except PermissionDenied:
            return False
    return False


def _user_has_module_perms(user, app_label):
    """
    A backend can raise `PermissionDenied` to short-circuit permission checking.
    """
    for backend in auth.get_backends():
        if not hasattr(backend, 'has_module_perms'):
            continue
        try:
            if backend.has_module_perms(user, app_label):
                return True
        except PermissionDenied:
            return False
    return False


@python_2_unicode_compatible
class Permission(models.Model):
    """
    The permissions system provides a way to assign permissions to specific
    users and groups of users.

    The permission system is used by the Django admin site, but may also be
    useful in your own code. The Django admin site uses permissions as follows:

        - The "add" permission limits the user's ability to view the "add" form
          and add an object.
        - The "change" permission limits a user's ability to view the change
          list, view the "change" form and change an object.
        - The "delete" permission limits the ability to delete an object.

    Permissions are set globally per type of object, not per specific object
    instance. It is possible to say "Mary may change news stories," but it's
    not currently possible to say "Mary may change news stories, but only the
    ones she created herself" or "Mary may only change news stories that have a
    certain status or publication date."

    Three basic permissions -- add, change and delete -- are automatically
    created for each Django model.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_('name'), max_length=255)
    content_type = models.ForeignKey(ContentType, related_name='ct_permission')
    codename = models.CharField(_('codename'), max_length=100)
    objects = PermissionManager()

    class Meta:
        verbose_name = _('permission')
        verbose_name_plural = _('permissions')
        unique_together = (('content_type', 'codename'),)
        ordering = ('content_type__app_label', 'content_type__model',
                    'codename')

    def __str__(self):
        return "%s | %s | %s" % (
            six.text_type(self.content_type.app_label),
            six.text_type(self.content_type),
            six.text_type(self.name))

    def natural_key(self):
        return (self.codename,) + self.content_type.natural_key()

    natural_key.dependencies = ['contenttypes.contenttype']


@python_2_unicode_compatible
class Group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_('name'), max_length=80, unique=True)
    isActive = models.BooleanField(default=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('permissions'), blank=True
    )

    objects = GroupManager()

    class Meta:
        verbose_name = _('group')
        verbose_name_plural = _('groups')

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

class User(AbstractBaseUser):
    username = models.CharField(max_length=20, null=False, blank=False, unique=True)
    #password = models.CharField(max_length=30, null=False, blank=False) clashes with the AbstractBaseUser field
    is_active = models.BooleanField(default=True)#este campo es necesario
    is_staff = models.BooleanField(default=False)#este campo es necesario
    is_superuser = models.BooleanField(default=False)#este campo es necesario
    fullName = models.CharField(max_length=70)
    email = models.EmailField(null=False, blank=False)
    chargeAtDepartment = models.CharField(max_length=50, null=False, blank=False)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    subManagement = models.ForeignKey(SubManagement, on_delete=models.CASCADE, null=True, blank=True)
    groups = models.ManyToManyField(Group, blank=True, related_name="user_set", related_query_name="user")
    user_permissions = models.ManyToManyField(Permission, blank=True, related_name="user_set",related_query_name="user")
    site = models.UUIDField(null=True, blank=True)
    sites = models.ManyToManyField(Site, blank=True, related_name="user_set", related_query_name="user")
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email','chargeAtDepartment','fullName']
    objects = UserManager()

    def __str__(self):
        return self.username

    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through their
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True
        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)

    def get_short_name(self):
        """Returns the short name for the user."""
        return self.username

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

class Vehicle(AbstractModel):
    name = models.CharField(max_length=50, null=False, blank=False)
    trademark = models.CharField(max_length=30)
    model = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    licensePlates = models.CharField(max_length=30, null=False, blank=False)
    classType = models.CharField(max_length=30, blank=True, null=True)
    serieNumber = models.CharField(max_length=50, blank=True, null=True)
    engineNumber = models.CharField(max_length=50, blank=True, null=True)
    capability = models.IntegerField(null=False, blank=False)
    cylinderCapacity = models.IntegerField(blank=True, null=True)
    avalaible = models.BooleanField(default=True)
    def __str__(self):
        return self.name

class Policy(AbstractModel, Dates):
    policyNumber = models.CharField(max_length=50, null=False, blank=False)
    insurance = models.CharField(max_length=200, null=False, blank=False)
    emergencyNumber = models.CharField(max_length=30)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    def __str__(self):
        return self.policyNumber

class Trip(AbstractModel, DatesTimes):
    passengers = models.IntegerField(null=False, blank=False)
    reasons = models.CharField(max_length=100, null=False, blank=False)
    createdBy = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    destinations = models.CharField(max_length=60, null=False, blank=False)
    groupLeader = models.CharField(max_length=70, blank=True, null=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    submanagement = models.ForeignKey(SubManagement, on_delete=models.CASCADE, null=True, blank=True)
    departmentPermission = models.CharField(max_length=10, default='No')
    submanagementPermission = models.CharField(max_length=10, default='No')
    departurePlace = models.CharField(max_length=100, null=True, blank=True)
    def __str__(self):
        return self.reasons

class DriverTrip(AbstractModel):
    tripExpenses = models.FloatField(null=False, blank=False)
    contractedDriver = models.CharField(max_length=300, null=True, blank=True)
    contractedVehicle = models.CharField(max_length=500, null=True, blank=True)
    driver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE, null=False, blank=False)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE, null=False, blank=False)
    def __str__(self):
        return str(self.tripExpenses)

class Maintenance(AbstractModel, DatesTimes):
    failures = models.CharField(max_length=150, blank=True, null=True)
    requestedServices= models.CharField(max_length=150, null=True, blank=True)
    mileage = models.IntegerField(null=False, blank=False)
    cost = models.FloatField(null=False, blank=False)
    bill = models.CharField(max_length=50, null=True, blank=True)
    workshop = models.CharField(max_length=50, blank=True, null=True)
    workshopPhone = models.CharField(max_length=30, blank=True, null=True)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE, null=False, blank=False)
    def __str__(self):
        return self.requestedServices

class Notification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.CharField(max_length=200, null=False, blank=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE, blank=True, null=True)
    read = models.BooleanField(default = False)
    dt = models.ForeignKey(DriverTrip, on_delete=models.CASCADE, blank=True, null=True)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE, blank=True, null=True)
    maintenance = models.ForeignKey(Maintenance, on_delete=models.CASCADE, blank=True, null=True)
    policy = models.ForeignKey(Policy, on_delete=models.CASCADE, blank=True, null=True)
    to = models.CharField(max_length=10, null=True, blank=True)
    def __str__(self):
        return self.description

# Create your models here.
