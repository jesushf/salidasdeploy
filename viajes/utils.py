
import json
import logging
import time
import requests
import uuid
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import slugify
from iron_mq import IronMQ

logger = logging.getLogger(__name__)


def send_message_queue(name_queue, the_message):
    try:
        mq = IronMQ(
            host="mq-aws-eu-west-1-1.iron.io",
            token=settings.IRON_IO['token'],
            project_id=settings.IRON_IO['project'],
            protocol='https', port=443,
            api_version=3,
            config_file=None
        )
        queue = mq.queue(name_queue)
        message = json.dumps(the_message)
        status = queue.post(message)
        logger.info("Message send to {} {}".format(name_queue, status))
        return True
    except Exception as e:
        logger.error("No post message to %s %s" % (name_queue, e))


def create_slug(value, instance, site=None):
    """
    Create slug of one instance
    :param value: name of the instance
    :param instance: Model to create slug
    :return: slug of the instance
    """
    try:
        params = {'slug': slugify(value)}
        if site:
            params.update({'site': site})
        instance.objects.get(**params)
        return slugify('{}{}'.format(value, int(time.time())))
    except ObjectDoesNotExist:
        return slugify(value)


def get_token():
    """
    generate a uuid
    :return: uuid valid
    """
    return uuid.uuid4()


def get_state_by_city(city_id):
    """

    :param city_id:
    :return:
    """
    if city_id is None or city_id == '':
        return None
    r = requests.get('{}cities/{}'.format(settings.HOST_GEO, city_id))
    if r.status_code != 200:
        return None
    return json.loads(r.text)['state']


def check_city(city_id):
    """
    Check if the city exist
    :param city_id: city id
    :return: None or id_city
    """
    if city_id == '' or city_id is None:
        return True
    r = requests.get('{}cities/{}'.format(settings.HOST_GEO, city_id))
    if r.status_code != 200:
        return False
    return True


def permission_exist(permission_id, list_permissions):
    """
    Check if the permission exist on list
    :param permission_id: id of the permissions to find
    :param list_permissions: list's permissions to mach
    :return:
    """
    for permission in list_permissions:
        if permission_id == permission['id']:
            return True
    return False


def get_user_permissions(obj):
    groups = obj.groups.all()
    response_permissions = []
    # add permissions from the groups
    for group in groups:
        permissions = group.permissions.all()
        for permission in permissions:
            if permission_exist(str(permission.id), response_permissions):
                continue
            response_permissions.append(
                {'id': str(permission.id), 'name': permission.name, 'codename': permission.codename}
            )
    return response_permissions or None
