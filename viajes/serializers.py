from django.contrib.admin.models import LogEntry
import logging
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from django.contrib import auth
import datetime
from .models import *
from viajes import utils
import django
from django.conf import settings
from django.core.mail import send_mail


class SubManagementSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubManagement
        fields = '__all__'

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'

class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = '__all__'

logger = logging.getLogger(__name__)

class UserSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()
    sites = serializers.PrimaryKeyRelatedField(many=True, queryset=Site.objects.all())
    groups = serializers.PrimaryKeyRelatedField(many=True, queryset=Group.objects.all())
    password2 = serializers.ReadOnlyField()

    class Meta:
        model = auth.get_user_model()
        fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True, 'required': False},
            'user_permissions': {'write_only': True, 'required': False},
        }

    def create(self, validated_data):
        if not validated_data.get('password'):
            raise ValidationError(_('The password not empty'))
        instance = super(UserSerializer, self).create(validated_data)
        instance.set_password(validated_data.get('password'))
        instance.save()
        logger.info(_('User {} created ok'.format(instance.username)))
        return instance

    def update(self, instance, validated_data):
        if validated_data.get('password'):
            instance.set_password(validated_data.pop('password'))
        logger.info(_('User {} update ok'.format(instance.username)))
        return super(UserSerializer, self).update(instance=instance, validated_data=validated_data)

    @classmethod
    def get_permissions(cls, obj):
        groups = obj.groups.all()
        response_permissions = []
        # add permissions from the groups
        for group in groups:
            permissions = group.permissions.all()
            for permission in permissions:
                if utils.permission_exist(permission.id, response_permissions):
                    continue
                response_permissions.append(PermissionSerializer(permission).data)
        # add permissions individuals
        for permission in obj.user_permissions.all():
            if not utils.permission_exist(permission.id, response_permissions):
                continue
            response_permissions.append(PermissionSerializer(permission).data)
        return response_permissions or None


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'isActive', 'createdAt', 'updatedAt', 'permissions']


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'

class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'


class PolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy
        fields = '__all__'

class TripSerializer(serializers.ModelSerializer):
    #createdBy = serializers.ReadOnlyField(source='User.username')
    #updatedBy = serializers.ReadOnlyField(source='User.username')
    class Meta:
        model = Trip
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        trip = super(TripSerializer, self).create(validated_data)
        trip.createdBy = user
        trip.createdAt = datetime.datetime.now()
        if user.department:
            trip.department = user.department
            dep = Department.objects.get(name=user.department)
            trip.submanagement = dep.submanagement
        else:
            trip.submanagement=user.subManagement
        trip.save()
        return trip

class DriverTripSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverTrip
        fields = '__all__'
        #createdBy = serializers.ReadOnlyField(source='createdBy.username')

class MaintenanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Maintenance
        fields = '__all__'
class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'
    def create(self, validated_data):
        notification = super(NotificationSerializer, self).create(validated_data)
        if notification.trip:
            trip = Trip.objects.get(reasons=notification.trip)
        if notification.to == 'dep':
            dep = Department.objects.get(name=trip.department)
            us = auth.get_user_model().objects.get(fullName=dep.boss)
            notification.user=us
        elif notification.to == 'sub':
            sub = SubManagement.objects.get(name=trip.submanagement)
            us = auth.get_user_model().objects.get(fullName=sub.boss)
            notification.user = us
        #elif notification.to == 'standar':
            #us =trip.createdBy
            #notification.user = us
        elif notification.to == 'recursos':
            sub = Department.objects.get(name='Recursos Materiales y Servicios')
            us = auth.get_user_model().objects.get(fullName=sub.boss)
            notification.user = us
        elif notification.to == 'driver':
            us = notification.user
        notification.save()

        send_mail('Notificación del sistema de control y salidas del ITJ', notification.description, settings.EMAIL_HOST_USER,
                  [us.email], fail_silently=False)
        return notification