from django.conf.urls import url
from django.views.decorators.csrf import ensure_csrf_cookie

from viajes import views
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = [
    url(r'^api/subs/$', ensure_csrf_cookie(views.SubmanagementList.as_view())),
    url(r'^api/subs/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.SubmanagementDetail.as_view())),
    url(r'^api/departments/$', ensure_csrf_cookie(views.DepartmentList.as_view())),
    url(r'^api/departments/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.DepartmentDetail.as_view())),
    url(r'^api/users/$', ensure_csrf_cookie(views.UserList.as_view())),
    url(r'^api/users/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.UserDetail.as_view())),
    url(r'^api/username/$', ensure_csrf_cookie(views.username), name='username'),
    url(r'^api/trips/$', ensure_csrf_cookie(views.TripList.as_view())),
    url(r'^api/trips/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.TripDetail.as_view())),
    url(r'^api/notifications/$', ensure_csrf_cookie(views.NotificationList.as_view())),
    url(r'^api/notifications/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.NotificationDetail.as_view())),
    url(r'^api/loginrec/$', ensure_csrf_cookie(views.LoginRecView.as_view())),
    url(r'^api/loginadmin/$', ensure_csrf_cookie(views.LoginAdmin.as_view())),
    url(r'^api/logindriver/$', ensure_csrf_cookie(views.LoginDriver.as_view())),
    url(r'^api/driver/trips/$', ensure_csrf_cookie(views.DriverTripListUser.as_view())),
    url(r'^api/logins/$', ensure_csrf_cookie(views.LoginStandar.as_view())),
    url(r'^api/loginsub/$', ensure_csrf_cookie(views.LoginSub.as_view())),
    url(r'^api/logindep/$', ensure_csrf_cookie(views.LoginDep.as_view())),
    url(r'^api/logout2/$', ensure_csrf_cookie(views.LogoutView.as_view())),
    url(r'^api/trip/vehicles/$', ensure_csrf_cookie(views.ListAvalaibleVehicle.as_view())),
    url(r'^api/trip/drivers/$', ensure_csrf_cookie(views.ListAvalaibleDriver.as_view())),
    url(r'^api/recursos/trips/$', ensure_csrf_cookie(views.TripListRecursos.as_view())),
    url(r'^api/vehicles/$', ensure_csrf_cookie(views.VehicleList.as_view())),
    url(r'^api/vehicles/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.VehicleDetail.as_view())),
    url(r'^api/maintenances/$', ensure_csrf_cookie(views.MaintenanceList.as_view())),
    url(r'^api/maintenances/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.MaintenanceDetail.as_view())),
    url(r'^api/drivertrips/$', ensure_csrf_cookie(views.DriverTripList.as_view())),
    url(r'^api/drivertrips/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.DriverTripDetail.as_view())),
    url(r'^api/policies/$', ensure_csrf_cookie(views.PolicyList.as_view())),
    url(r'^api/policies/(?P<pk>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.PolicyDetail.as_view())),
    url(r'^api/user/trips/$', ensure_csrf_cookie(views.TripListUser.as_view())),
    url(r'^api/dep/trips/$', ensure_csrf_cookie(views.TripListDep.as_view())),
    url(r'^api/sub/trips/$', ensure_csrf_cookie(views.TripListSub.as_view())),
    url(r'^api/user/notifications/$', ensure_csrf_cookie(views.NotificationListUser.as_view())),
    url(r'^api/documentpdf/(?P<id>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.pdf2), name='pdf2'),
    url(r'^api/documentpdfs/(?P<id>[a-z0-9 -]+)/$', ensure_csrf_cookie(views.pdf), name='pdf'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
